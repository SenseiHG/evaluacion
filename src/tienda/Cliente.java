
package tienda;


public class Cliente extends Reserva{
    String Cedula;
    String Nombres;
    String Apellido;
    String email;
    String Contraseña;
    
    
    
    public String ImprimirDatos(){
        return "Cedula: "+ Cedula + "Nombres: "+Nombres +"Apellido: "+"email: "+email+"Contraseña: "+Contraseña;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }
    
    
}
